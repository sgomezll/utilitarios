package cl.utilities.main.exception;

/**
 * Superclase de todas las excepciones de los utilitarios. Cada una de éstas debe extenderla.
 *
 * @author Sebastián Gómez
 */
public abstract class UtilitiesException extends Exception {

    private static final long serialVersionUID = 8620532727348117073L;

    /**
     * Constructor por defecto, éste acepta un mensaje descriptivo al error asociado.
     *
     * @param message Mensaje del por qué ocurrió la excepción.
     */
    public UtilitiesException(String message) {
        super(message);
    }
}
