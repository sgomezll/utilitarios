package cl.utilities.main.exception;

public class WebserviceUtilException extends UtilitiesException {

    private static final long serialVersionUID = -2854741332306633519L;

    /**
     * Constructor por defecto, éste acepta un mensaje descriptivo al error asociado.
     *
     * @param message Mensaje del por qué ocurrió la excepción.
     */
    public WebserviceUtilException(String message) {
        super(message);
    }
}
