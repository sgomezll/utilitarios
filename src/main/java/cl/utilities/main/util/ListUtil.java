package cl.utilities.main.util;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Utilitario para Listas
 *
 * @author Sebastián Gómez
 */
public final class ListUtil {

    /**
     * Constructor privado y que impide la creaci&oacute;n de instancias para este utilitario.
     */
    private ListUtil() {
        throw new AssertionError(
            "This class is a utilitary, you can't get a instance for this class: " + this.getClass().getName());
    }

    /**
     * Obtiene una concatenaci&oacute;n de String de una lista de String.
     *
     * @param stringList Lista de {@link String}.
     * @return Concatenaci&oacute;n de toda la lista de {@link String} con un separador por defecto: "<code>, </code>" y
     * sin mensaje cuando la lista es vac&iacute;a.
     */
    public static String obtainStringFromList(List<String> stringList) {
        return ListUtil.obtainStringFromList(stringList, ", ", "");
    }

    /**
     * Obtiene una concatenaci&oacute;n de String de una lista de String.
     *
     * @param stringList Lista de {@link String}.
     * @param separator Separador de la lista.
     * @return Concatenaci&oacute;n de toda la lista de {@link String} con el separador definido y sin mensaje cuando la
     * lista es vac&iacute;a.
     */
    public static String obtainStringFromList(List<String> stringList, String separator) {
        return ListUtil.obtainStringFromList(stringList, separator, "");
    }

    /**
     * Obtiene una concatenaci&oacute;n de String de una lista de String.
     * <br><br>
     * <i>NOTA</i>: Ninguno de los objetos puede venir nulo.
     *
     * @param stringList Lista de {@link String}.
     * @param separator Separador de la lista.
     * @param notFoundMessage Mensaje si la lista est&aacute; vac&iacute;a.
     * @return Concatenaci&oacute;n de toda la lista de {@link String} con separador y mensaje definido.
     */
    public static String obtainStringFromList(List<String> stringList, String separator, String notFoundMessage) {
        Objects.requireNonNull(stringList, "The list cannot be null");
        Objects.requireNonNull(separator, "The separator cannot be null");
        Objects.requireNonNull(notFoundMessage, "The message cannot be null");
        return stringList.stream()
            .reduce((s, s2) -> s + separator + s2)
            .orElse(notFoundMessage);
    }

    /**
     * Convierte una lista numérica ({@link Number}) en una lista de {@link String}
     *
     * @param numberList Lista de números.
     * @return Lista convertida en String.
     */
    public static List<String> convertNumberListToStringList(List<? extends Number> numberList) {
        Objects.requireNonNull(numberList, "The list cannot be null");
        return numberList.stream()
            .map(Object::toString)
            .collect(Collectors.toList());
    }

    /**
     * Dada una lista de valores, encontrar todos los n&uacute;meros disponibles (los que no est&eacute;n en la segunda
     * lista) dentro de un rango A a uno B excluyente, si se quiere incluir el valor B, invocar a {@link
     * ListUtil#generateAvailableValues(List, Integer, Integer, boolean)}
     *
     * @param existValues Valores a excluir.
     * @param init Valor de inicio para la secuencia.
     * @param end Valor de fin para la secuencia.
     * @return La lista con los valores disponibles (que no existen en la lista original).
     */
    public static List<Integer> generateAvailableValues(List<Integer> existValues, Integer init, Integer end) {
        return generateAvailableValues(existValues, init, end, false);
    }

    /**
     * Dada una lista de valores, encuentra el valor mínimo disponible (los que no est&eacute;n en la segunda lista)
     * dentro de un rango A a uno B excluyente, si se quiere incluir el valor B, invocar a {@link
     * ListUtil#findMinAvailable(List, Integer, Integer, boolean)}
     *
     * @param existValues Valores a excluir.
     * @param init Valor de inicio para la secuencia.
     * @param end Valor de fin para la secuencia.
     * @return El valor mínimo disponible que no existen en la lista original.
     */
    public static Integer findMinAvailable(List<Integer> existValues, Integer init, Integer end) {
        return findMinAvailable(existValues, init, end, false);
    }

    /**
     * Dada una lista de valores, encontrar todos los n&uacute;meros disponibles (los que no est&eacute;n en la segunda
     * lista) dentro de un rango A a uno B.
     *
     * @param existValues Valores a excluir.
     * @param init Valor de inicio para la secuencia.
     * @param end Valor de fin para la secuencia.
     * @param isInclusive Indica si el valor final de la secuencia está incluido o no.
     * @return La lista con los valores disponibles (que no existen en la lista original).
     */
    public static List<Integer> generateAvailableValues(List<Integer> existValues,
                                                        final Integer init,
                                                        final Integer end,
                                                        boolean isInclusive) {
        Objects.requireNonNull(init, "The init sequence cannot be null");
        Objects.requireNonNull(end, "The end sequence cannot be null");
        return Optional.of(isInclusive)
            .filter(i -> i)
            .map(i -> IntStream.rangeClosed(init, end)
                .parallel()
                .boxed()
                .filter(n -> !existValues.contains(n))
                .collect(Collectors.toList()))
            .orElseGet(() -> IntStream.range(init, end)
                .parallel()
                .boxed()
                .filter(n -> !existValues.contains(n))
                .collect(Collectors.toList()));
    }

    /**
     * Dada una lista de valores, encuentra el valor mínimo disponible (los que no est&eacute;n en la segunda lista)
     * dentro de un rango A a uno B.
     *
     * @param existValues Valores a excluir.
     * @param init Valor de inicio para la secuencia.
     * @param end Valor de fin para la secuencia.
     * @param isInclusive Indica si el valor final de la secuencia está incluido o no.
     * @return El valor mínimo disponible que no existen en la lista original.
     */
    public static Integer findMinAvailable(List<Integer> existValues,
                                           final Integer init,
                                           final Integer end,
                                           boolean isInclusive) {
        Objects.requireNonNull(existValues, "The exists values cannot be null");
        Objects.requireNonNull(init, "The init sequence cannot be null");
        Objects.requireNonNull(end, "The end sequence cannot be null");
        return Optional.of(isInclusive)
            .filter(i -> i)
            .flatMap(i -> IntStream.rangeClosed(init, end)
                .boxed()
                .filter(n -> !existValues.contains(n))
                .min(Integer::compareTo))
            .orElseGet(() -> IntStream.range(init, end)
                .boxed()
                .filter(n -> !existValues.contains(n))
                .min(Integer::compareTo)
                .orElse(null));
    }

}
