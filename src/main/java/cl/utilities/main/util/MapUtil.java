package cl.utilities.main.util;

import java.util.Map;
import java.util.Objects;

/**
 * Utilitario que trabaja con diferentes funciones sobre los mapas.
 *
 * @author Sebastián Gómez
 */
public final class MapUtil {

    /**
     * Constructor privado y que impide la creaci&oacute;n de instancias para este utilitario.
     */
    private MapUtil() {
        throw new AssertionError(
            "This class is a utilitary, you can't get a instance for this class: " + this.getClass().getName());
    }

    /**
     * Obtiene la llave (key) asociada al valor, si tiene m&aacute;s de un valor asociado, retornar&aacute; la
     * &uacute;ltima llave (key).
     *
     * @param map Mapa objetivo.
     * @param value Valor que se busca.
     * @param <K> Tipo de objeto gen&eacute;rico para la llave (key)
     * @param <V> Tipo de objeto gen&eacute;rico para el valor (value)
     * @return La llave asociada al valor buscado, si el valor est&aacute; asociado a m&aacute;s de una llave,
     * retornar&aacute; la &uacute;ltima, si el valor buscado no se encuentra, retornar&aacute; {@code null}.
     */
    public static <K, V> K findKeyByValue(final Map<K, V> map, final V value) {
        Objects.requireNonNull(map, "The origin map cannot be null");
        return map.entrySet()
            .stream()
            .parallel()
            .filter(es -> Objects.equals(es.getValue(), value))
            .map(Map.Entry::getKey)
            .reduce((k1, k2) -> k2)
            .orElse(null);
    }
}
