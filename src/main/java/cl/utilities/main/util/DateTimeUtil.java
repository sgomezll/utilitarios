package cl.utilities.main.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Utilitario que opera con fechas y unidades de tiempos.
 * <br><br>
 * <b>Nota</b>: La clase no puede ser extendida (final) dado que es un utilitario.
 *
 * @author Sebastián Gómez
 */
public final class DateTimeUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(DateTimeUtil.class);

    /* Mensajes */
    private static final String ERROR_PARSING_DATE = "Error parsing date";
    private static final String THE_DATE_FORMAT_CANNOT_BE_NULL = "The date format cannot be null";
    private static final String THE_INPUT_DATE_CANNOT_BE_NULL = "The input date cannot be null";
    private static final String THE_DATE_TOC_CONVERT_CANNOT_BE_NULL = "The date toc convert cannot be null";
    private static final String THE_CHRONO_UNIT_CANNOT_BE_NULL = "The chrono unit cannot be null";
    private static final String THE_STRING_DATE_CANNOT_BE_NULL = "The string date cannot be null";

    private DateTimeUtil() {
        throw new AssertionError(
            "You can't get a instance for " + DateTimeUtil.class.getName() + ", because is a utilitary.");
    }

    /**
     * Obtiene la fecha actual en UTC (Universal Time Coordinated)
     *
     * @return Fecha actual en UTC.
     */
    public static Date getCurrentDateUTC() {
        LocalDateTime utc = LocalDateTime.now(ZoneId.of(ZoneOffset.UTC.getId()));
        return Date.from(utc.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Obtiene la fecha actual del sistema.
     *
     * @return Fecha actual del sistema.
     */
    public static Date getCurrentDateSystemTime() {
        LocalDateTime localDateTime = LocalDateTime.now(ZoneId.systemDefault());
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Obtiene la fecha actual de Chile. <br><br>
     * <b>Nota</b>: Esta funci&oacute;n coincide con {@link DateTimeUtil#getCurrentDateSystemTime()} si el sistema
     * est&aacute;
     * instalado en Chile bajo el horario <i>America/Santiago</i>. De lo contrario, esta funci&oacute;n se asegura de
     * utilizar el huso horario de Chile.
     *
     * @return Fecha actual de Chile (UTC -4/UTC -3).
     */
    public static Date getCurrentDateChileanTime() {
        LocalDateTime localDateTime = LocalDateTime.now(ZoneId.of("America/Santiago"));
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Convierte una fecha expresada en un string hacia un objeto de tipo {@link LocalDateTime} usando las nuevas
     * funciones de Java 8.
     *
     * @param dateStr Fecha en un objeto {@link String}.
     * @param format Formato que contiene la fecha de entrada.
     * @return Fecha parseada desde un string.
     */
    public static LocalDateTime stringToLocalDateTime(String dateStr, String format) {
        Objects.requireNonNull(dateStr, THE_STRING_DATE_CANNOT_BE_NULL);
        Objects.requireNonNull(format, "The format date cannot be null");
        try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);
            return LocalDateTime.parse(dateStr, dtf);
        } catch (DateTimeParseException e) {
            LOGGER.error(ERROR_PARSING_DATE, e);
        }
        return null;
    }

    /**
     * Convierte una fecha expresada en un string hacia un objeto de tipo {@link Date}.
     *
     * @param dateStr Fecha en un objeto {@link String}.
     * @param format Formato que contiene la fecha de entrada.
     * @return Fecha parseada desde un string.
     * @deprecated Esta funci&oacute;n debe utilizarse s&oacute;lo en sistemas que no soporten otros objetos de fechas
     * distinto a {@link Date} o sistemas legados (legacy).
     */
    @Deprecated
    public static Date stringToDate(String dateStr, String format) {
        if (dateStr == null || format == null) {
            String message = String.format("An illegal argument value found. No argument can be null. dateStr: %s, format: %s", dateStr, format);
            throw new IllegalArgumentException(message);
        }
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return sdf.parse(dateStr);
        } catch (ParseException e) {
            LOGGER.error(ERROR_PARSING_DATE, e);
        }
        return null;
    }

    /**
     * Para funcionalidades legadas (<i>legacy</i>) que no soporten el nuevo formato, convierte una fecha en el formato
     * {@link LocalDateTime} en {@link Date}, habilitando un modo de retrocompatibilidad.
     * <br><br>
     * <b><i>Nota</i></b>: El uso de esta funci&oacute;n debe estar <b>limitada</b> para trabajar con otras funciones
     * que no sean compatibles con los nuevos formatos de fecha aparecidos desde Java 8, como por ejemplo
     * inserci&oacute;n a base de datos. Para el trabajo interno se recomienda evitar la invocaci&oacute;n de esta
     * funcionalidad.
     *
     * @param localDateTime Fecha con el formato nuevo.
     * @return Fecha con el formato legacy {@link Date}.
     */
    public static Date localDateTimeToDate(LocalDateTime localDateTime) {
        return Optional.ofNullable(localDateTime)
            .map(l -> Date.from(l.atZone(ZoneId.systemDefault()).toInstant()))
            .orElse(null);
    }

    /**
     * Convierte un objeto de tipo {@link Date} a {@link LocalDateTime}.
     *
     * @param date Fecha con el formato legacy.
     * @return Fecha con el nuevo formato.
     */
    public static LocalDateTime dateToLocalDateTime(Date date) {
        return Optional.ofNullable(date)
            .map(d -> d.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
            .orElse(null);
    }

    /**
     * Comprueba si una fecha con hora en particular est&aacute; entre dos fechas con horas inclusiva.
     *
     * @param testedDateTime Fecha a comprobar.
     * @param initDateTime Fecha de inicio.
     * @param endDateTime Fecha de t&eacute;rmino.
     * @return <b><i>true</i></b>: Si la fecha a comprobar est&aacute; dentro del rango de las fechas de inicio y
     * fin.<br>
     * <b><i>false</i></b>: En caso contrario.
     */
    public static Boolean isBetweenTwoDates(LocalDateTime testedDateTime, LocalDateTime initDateTime, LocalDateTime endDateTime) {
        Objects.requireNonNull(testedDateTime, "Checked date cannot be null");
        Objects.requireNonNull(initDateTime, "Initial date cannot be null");
        Objects.requireNonNull(endDateTime, "Ended date cannot be null");
        return initDateTime.isBefore(testedDateTime) && endDateTime.isAfter(testedDateTime) ||
            (initDateTime.equals(testedDateTime) || endDateTime.equals(testedDateTime));
    }

    /**
     * Comprueba si una hora (tiempo) en particular est&aacute; entre dos tiempos (horas) inclusiva al tiempo inicial,
     * excluyente al tiempo final.
     * <br><br>
     * <code>
     * Si los rangos definidos son:<br> inicio -> 12:00 hrs<br> final -> 13:00 hrs<br>
     * <br>
     * Cuando se comprueba<br> 11:59 hrs entonces retornará <b>false</b><br> 12:00 hrs entonces retornará
     * <b>true</b><br> 12:59 hrs entonces retornará <b>true</b><br> 13:00 hrs entonces retornará <b>false</b><br>
     * </code>
     *
     * @param testedTime Tiempo a comprobar.
     * @param initTime Tiempo de inicio.
     * @param endTime Tiempo de t&eacute;rmino.
     * @return <b><i>true</i></b>: Si el tiempo a comprobar est&aacute; dentro del rango del tiempo inicio y fin.<br>
     * <b><i>false</i></b>: En caso contrario.
     */
    public static Boolean isBetweenTwoLocalTime(LocalTime testedTime, LocalTime initTime, LocalTime endTime) {
        Objects.requireNonNull(testedTime, "Tested time cannot be null");
        Objects.requireNonNull(initTime, "Init time cannot be null");
        Objects.requireNonNull(endTime, "End time cannot be null");
        return (initTime.isBefore(testedTime) && endTime.isAfter(testedTime)) || initTime.equals(testedTime);
    }

    /**
     * Convierte una fecha de tipo {@link Date} en LocalTime, extray&eacute;ndo solo la parte horaria.
     *
     * @param date Fecha a convertir (formato legacy)
     * @return La parte horaria (tiempo) de una fecha.
     */
    public static LocalTime dateToLocalTime(Date date) {
        return Optional.ofNullable(DateTimeUtil.dateToLocalDateTime(date))
            .map(LocalDateTime::toLocalTime)
            .orElse(null);
    }

    /**
     * Parsea una fecha de tipo String con un formato determinado, en un objeto de tipo {@link LocalDateTime}
     *
     * @param dateStr Fecha de entrada.
     * @param format Formato de la fecha.
     * @return Objeto {@link LocalDateTime} con los valores.
     * @see DateTimeFormatter
     */
    public static LocalDateTime parse(String dateStr, String format) {
        Objects.requireNonNull(dateStr, THE_INPUT_DATE_CANNOT_BE_NULL);
        Objects.requireNonNull(format, THE_DATE_FORMAT_CANNOT_BE_NULL);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
        return LocalDateTime.parse(dateStr,
            dateTimeFormatter);
    }

    /**
     * Convierte una fecha {@link LocalDateTime} en String usando el formato dado.
     *
     * @param date Fecha de entrada.
     * @param format Formato de la fecha a convertir.
     * @return Fecha representada en {@link String}
     */
    public static String dateToString(LocalDateTime date, String format) {
        Objects.requireNonNull(date, THE_DATE_TOC_CONVERT_CANNOT_BE_NULL);
        Objects.requireNonNull(format, THE_DATE_FORMAT_CANNOT_BE_NULL);
        return date.format(DateTimeFormatter.ofPattern(format));
    }

    /**
     * Convierte una fecha {@link Date} en String usando el formato dado.
     *
     * @param date Fecha de entrada.
     * @param format Formato de la fecha a convertir.
     * @return Fecha representada en {@link String}
     */
    public static String dateToString(Date date, String format) {
        Objects.requireNonNull(date, THE_DATE_TOC_CONVERT_CANNOT_BE_NULL);
        Objects.requireNonNull(format, THE_DATE_FORMAT_CANNOT_BE_NULL);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

    /**
     * Calcula la diferencia entre dos fechas.
     *
     * @param start Fecha de inicio.
     * @param end Fecha de fin.
     * @param chronoUnit Unidad de tiempo a calcular la diferencia.
     * @return La diferencia entre las dos fechas.
     */
    public static Long diff(LocalDateTime start, LocalDateTime end, ChronoUnit chronoUnit) {
        Objects.requireNonNull(chronoUnit, THE_CHRONO_UNIT_CANNOT_BE_NULL);
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime startDate = Optional.ofNullable(start)
            .orElse(now);
        LocalDateTime endDate = Optional.ofNullable(end)
            .orElse(now);
        return chronoUnit.between(startDate, endDate);
    }

    /**
     * Calcula la diferencia entre dos fechas
     *
     * @param start Fecha de inicio.
     * @param end Fecha de fin.
     * @param chronoUnit Unidad de tiempo a calcular la diferencia.
     * @return La diferencia entre las dos fechas.
     */
    public static Long diffString(String start, String end, ChronoUnit chronoUnit, final String format) {
        Objects.requireNonNull(chronoUnit, THE_CHRONO_UNIT_CANNOT_BE_NULL);
        Objects.requireNonNull(format, THE_DATE_FORMAT_CANNOT_BE_NULL);
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime startDate = Optional.ofNullable(start)
            .map(sd -> DateTimeUtil.stringToLocalDateTime(sd, format))
            .orElse(now);
        LocalDateTime endDate = Optional.ofNullable(end)
            .map(ed -> DateTimeUtil.stringToLocalDateTime(ed, format))
            .orElse(now);
        return DateTimeUtil.diff(startDate, endDate, chronoUnit);
    }

    /**
     * Obtiene los elementos de d&iacute;a, mes y a&ntilde;o de una fecha. La fecha debe cumplir con tener
     * <b>S&Oacute;LO</b> esos tres elementos, si no cumple con uno de ellos, entonces la operaci&oacute;n
     * fallar&aacute;.
     *
     * @param dateStr Fecha a obtener los tres elementos: d&iacute;a, mes, a&ntilde;o.
     * @param separator Separador de la fecha.
     * @throws IllegalArgumentException En caso que la fecha no tenga el formato v&aacute;lido, es decir, que no
     * contenga un d&iacute;a, mes y a&ntilde;o definido de acuerdo al separador; En caso que la fecha venga nula.
     * @return Los tres elementos dentro de un array para una fecha: 0 -> d&iacute;a; 1 -> mes; 2 -> a&ntilde;o.
     */
    public static int[] obtainElementsFromDate(final String dateStr, final char separator) {
        String[] dateComponents = Optional.ofNullable(dateStr)
            .filter(d -> !d.contains(" ")) // -> Previene que la fecha contenga espacios.
            .filter(d -> !d.contains(":")) // -> Previene que la fecha contenga separador de hora.
            .map(d -> d.split(String.valueOf(separator)))
            .filter(d -> d.length == 3)
            .orElseThrow(() -> new IllegalArgumentException("La fecha indicada no parece tener un formato válido: " + dateStr + ", con el separador: " + separator));
        int[] posDate = new int[3];
        posDate[0] = Integer.parseInt(dateComponents[0]); // -> día
        posDate[1] = Integer.parseInt(dateComponents[1]); // -> mes
        posDate[2] = Integer.parseInt(dateComponents[2]); // -> año
        return posDate;
    }

    /**
     * Convierte una fecha y hora en solo fecha, s&oacute;lo evaluar&aacute; que contenga un espacio entre el a&ntilde;o
     * y la hora de una fecha, es agn&oacute;stico al separador.
     *
     * @param dateStr Fecha y hora a convertir.
     * @return S&oacute;lo la fecha.
     */
    public static String convertDateTimeToDate(String dateStr) {
        return Optional.ofNullable(dateStr)
            .map(d -> d.split(" "))
            .map(d -> d[0])
            .orElseThrow(() -> new IllegalArgumentException("La fecha no pudo convertirse: " + dateStr));
    }

    /**
     * Convierte un calendario en un objeto {@link LocalDateTime}.
     *
     * @param cal Calendario a convertir.
     * @return El calendario convertido en {@link LocalDateTime}.
     */
    public static LocalDateTime convertCalendarIntoLocalDateTime(Calendar cal) {
        Objects.requireNonNull(cal, "The calendar cannot be null");
        return dateToLocalDateTime(cal.getTime());
    }

    /**
     * Calcula la diferencia entre dos calendarios dado una unidad de tiempo {@link ChronoUnit}.
     *
     * @param init Calendario con la fecha inicial.
     * @param end Calendario con la fecha final.
     * @param chronoUnit Unidad de tiempo en la que se realizará la diferencia entre ambos calendarios.
     * @return La diferencia de tiempo entre dos calendarios dado el {@link ChronoUnit} especificado.
     * @throws NullPointerException En caso que cualquier parámetro llegue nulo.
     */
    public static Long diffCalendar(Calendar init, Calendar end, ChronoUnit chronoUnit) {
        Objects.requireNonNull(chronoUnit, THE_CHRONO_UNIT_CANNOT_BE_NULL);
        final Date dateInit = Optional.ofNullable(init)
            .map(Calendar::getTime)
            .orElseThrow(() -> new NullPointerException("The init calendar cannot be null"));
        final Date dateEnd = Optional.ofNullable(end)
            .map(Calendar::getTime)
            .orElseThrow(() -> new NullPointerException("The end calendar cannot be null"));
        return diff(dateToLocalDateTime(dateInit), dateToLocalDateTime(dateEnd), chronoUnit);
    }

    /**
     * Evalúa que la fecha ingresada (al momento de hacer la invocación) coincida con el verdadero "ayer" al momento
     * de hacer la invocación, dado que sobrecarga la función {@link DateTimeUtil#isYesterday(Date, Date)} que evalúa
     * si la segunda fecha es el ayer de la primera fecha. Esta función retorna de manera dinámica el resultado
     * puesto que depende del momento en que se invocó.
     *
     * @param yesterday Fecha a evaluar si es el ayer de "hoy".
     * @return TRUE: Si la fecha ingresada corresponde al ayer de hoy, es decir, del momento en que se invocó; FALSE:
     * En caso contrario.
     */
    public static Boolean isYesterday(Date yesterday) {
        return isYesterday(new Date(), yesterday);
    }

    /**
     * Comprueba que dada la primera fecha, la segunda corresponda al "ayer" de la primera.
     *
     * @param now Fecha a evaluar.
     * @param yesterday El ayer de la primera fecha a evaluar.
     * @return TRUE: Si la segunda fecha corresponde al ayer de la primera; FALSE: En caso contrario.
     */
    public static Boolean isYesterday(Date now, Date yesterday) {
        return isPast(now, yesterday, ChronoUnit.DAYS, 1);
    }

    /**
     * Evalua el pasado estricto de la segunda fecha con respecto a la primera tomando en consideración la diferencia
     * en {@link ChronoUnit#NANOS} (nanosegundos).
     *
     * @param firstDate Primera fecha a evaluar.
     * @param pastDate La fecha en pasado con respecto a la primera.
     * @return TRUE: Si la segunda fecha es "pasado" de la primera; FALSE: En caso contrario.
     */
    public static Boolean isPast(final Date firstDate, final Date pastDate) {
        return isPast(firstDate, pastDate, ChronoUnit.NANOS, 1);
    }

    /**
     * Comprueba que dada la primera fecha, la segunda sea una fecha pasada con respecto a la primera dada una unidad
     * de tiempo determinada, que puede ser desde milisegundos hasta milenios.
     *
     * @param firstDate Primera fecha a evaluar.
     * @param pastDate La fecha en pasado con respecto a la primera.
     * @param temporalUnit Unidad de tiempo a evaluar.
     * @param temporal Cantidad de tiempo a considerar si es pasado o no (por ejemplo podemos considerar pasado a
     * partir de los días como a partir de los milisegundos).
     * @return TRUE: Si la segunda fecha correspondiente al pasado de la primera es "pasado"; FALSE: En caso contrario.
     */
    public static Boolean isPast(final Date firstDate,
                                 final Date pastDate,
                                 final TemporalUnit temporalUnit,
                                 final Integer temporal) {
        Objects.requireNonNull(firstDate, "The first date date entered cannot be null");
        Objects.requireNonNull(pastDate, "The past date date entered cannot be null");
        final Supplier<? extends RuntimeException> dateCannotBeEvaluatedException = () -> new IllegalArgumentException("La " +
            "fecha ingresada no se puede evaluar");
        final LocalDateTime truePast = Optional.ofNullable(dateToLocalDateTime(firstDate))
            .map(d -> d.minus(temporal, temporalUnit))
            .orElseThrow(dateCannotBeEvaluatedException);
        final LocalDateTime evaluatedPast = Optional.ofNullable(dateToLocalDateTime(pastDate))
            .orElseThrow(dateCannotBeEvaluatedException);
        if (!temporalUnit.isDateBased()) {
            return evaluatedPast.isBefore(truePast) || evaluatedPast.isEqual(truePast);
        }
        final LocalDate firstLocalDate = truePast.toLocalDate();
        final LocalDate pastLocalDate = evaluatedPast.toLocalDate();
        return pastLocalDate.isEqual(firstLocalDate);
    }

    /**
     * Evalúa que la fecha ingresada (al momento de hacer la invocación) coincida con el verdadero "mañana" al momento
     * de hacer la invocación, dado que sobrecarga la función {@link DateTimeUtil#isTomorrow(Date, Date)} que evalúa
     * si la segunda fecha es el mañana de la primera fecha. Esta función retorna de manera dinámica el resultado
     * puesto que depende del momento en que se invocó.
     *
     * @param tomorrow Fecha a evaluar si es el mañana de "hoy".
     * @return TRUE: Si la fecha ingresada corresponde al mañana de hoy, es decir, del momento en que se invocó; FALSE:
     * En caso contrario.
     */
    public static Boolean isTomorrow(Date tomorrow) {
        return isTomorrow(new Date(), tomorrow);
    }

    /**
     * Comprueba que dada la primera fecha, la segunda corresponda al "mañana" de la primera.
     *
     * @param now Fecha a evaluar.
     * @param tomorrow El ayer de la primera fecha a evaluar.
     * @return TRUE: Si la segunda fecha corresponde al ayer de la primera; FALSE: En caso contrario.
     */
    public static Boolean isTomorrow(Date now, Date tomorrow) {
        return isFuture(now, tomorrow, ChronoUnit.DAYS, 1);
    }

    /**
     * Evalua el futuro estricto de la segunda fecha con respecto a la primera tomando en consideración la diferencia
     * en {@link ChronoUnit#NANOS} (nanosegundos).
     *
     * @param firstDate Primera fecha a evaluar.
     * @param futureDate La fecha en futuro con respecto a la primera.
     * @return TRUE: Si la segunda fecha es "futuro" de la primera; FALSE: En caso contrario.
     */
    public static Boolean isFuture(Date firstDate, Date futureDate) {
        return isFuture(firstDate, futureDate, ChronoUnit.NANOS, 1);
    }

    /**
     * Comprueba que dada la primera fecha, la segunda sea una fecha futura con respecto a la primera dada una unidad
     * de tiempo determinada, que puede ser desde nanosegundos hasta milenios.
     *
     * @param firstDate Primera fecha a evaluar.
     * @param futureDate La fecha en futuro con respecto a la primera.
     * @param temporalUnit Unidad de tiempo a evaluar.
     * @param temporal Cantidad de tiempo a considerar si es futuro o no (por ejemplo podemos considerar futuro a
     * partir de los días como a partir de los nanosegundos).
     * @return TRUE: Si la segunda fecha correspondiente al futuro de la primera es "futuro"; FALSE: En caso contrario.
     */
    public static Boolean isFuture(final Date firstDate,
                                   final Date futureDate,
                                   final TemporalUnit temporalUnit,
                                   final Integer temporal) {
        Objects.requireNonNull(firstDate, "The first date date entered cannot be null");
        Objects.requireNonNull(futureDate, "The past date date entered cannot be null");
        final Supplier<? extends RuntimeException> dateCannotBeEvaluatedException = () -> new IllegalArgumentException("La " +
            "fecha ingresada no se puede evaluar");
        final LocalDateTime truePast = Optional.ofNullable(dateToLocalDateTime(firstDate))
            .map(d -> d.plus(temporal, temporalUnit))
            .orElseThrow(dateCannotBeEvaluatedException);
        final LocalDateTime evaluatedPast = Optional.ofNullable(dateToLocalDateTime(futureDate))
            .orElseThrow(dateCannotBeEvaluatedException);
        if (!temporalUnit.isDateBased()) {
            return evaluatedPast.isAfter(truePast) || evaluatedPast.isEqual(truePast);
        }
        final LocalDate firstLocalDate = truePast.toLocalDate();
        final LocalDate pastLocalDate = evaluatedPast.toLocalDate();
        return pastLocalDate.isEqual(firstLocalDate);
    }
}
