package cl.utilities.main.util;

import cl.utilities.main.enums.ErrorType;

import java.util.Optional;

/**
 * Utilitario que facilita extraer la informaci&oacute;n de las excepciones.
 *
 * @author Sebastián Gómez
 */
public final class ExceptionUtil {

    /**
     * Umbral para el largo del nombre completo de la clase (fully qualified class name).
     */
    private static final int THRESHOLD_FULLY_QUALIFIED_CLASS_NAME = 35;

    /**
     * Umbral para el largo completo del mensaje.
     */
    private static final int THRESHOLD_COMPLETE_MESSAGE = 80;

    /**
     * Constructor privado. Es un utilitario.
     */
    private ExceptionUtil() {
        throw new AssertionError(this.getClass().getName() + " es un utilitario y no se puede instanciar.");
    }

    /**
     * Formatea la salida de una excepci&oacute;n indicando el nombre de &eacute;sta, la clase, la l&iacute;nea y la
     * funci&oacute;n asociada, teniendo un formato autom&aacute;tico de acuerdo a la longitud del texto obtenido como
     * sigue: <br><br>
     * <code>
     * (Completo) <br> {ExceptionName} - {CompleteClassName}:{LineException} [M: {MethodName}] <br><br>
     * <p>
     * (Simple) <br> {ExceptionName} - {SimpleClassName}:{LineException} [M: {MethodName}] <br><br>
     * <p>
     * (M&iacute;nimo) <br> {ExceptionName} - {SimpleClassName}:{LineException}<br>
     * </code> <br>
     * Ejemplo: <br>
     * <code>
     * NullPointerException - cl.bancochile.portal.privado.persona.lcredito.configuracionlineacredito.service.impl
     * .ConfiguracionLineaServiceImpl.java:210 [M: getDatosCliente]
     * </code> <br><br>
     * Si el nombre cualificado de la clase es muy grande de acuerdo al umbral especificado en {@link
     * ExceptionUtil#THRESHOLD_FULLY_QUALIFIED_CLASS_NAME} entonces se usar&aacute; el nombre simple de la
     * clase/fichero del error:<br>
     * <code>
     * NullPointerException - ConfiguracionLineaServiceImpl.java:210 [M: getDatosCliente]
     * </code> <br><br>
     * Si el texto final tiene m&aacute;s caracteres de acuerdo a los especificado en {@link
     * ExceptionUtil#THRESHOLD_COMPLETE_MESSAGE} entonces se usar&aacute; el nombre simple de la clase/fichero y sin la
     * informaci&oacute;n de la funci&oacute;n: <br>
     * <code>
     * NullPointerException - ConfiguracionLineaServiceImpl.java:210
     * </code> <br><br>
     * En caso que no venga la excepci&oacute;n ({@code null}) el formato se mostrar&aacute; de la siguiente manera:
     * <br>
     * <code>
     * ? - UnknownClass:-- [M: UnknownMethod]
     * </code>
     *
     * @param e Excepci&oacute;n a formatear.
     * @param <X> Tipo de excepci&oacute;n; se excluyen los {@link Error}.
     * @return La excepci&oacute;n formateada.
     */
    public static <X extends Exception> String formattedRootCause(X e) {
        final Throwable rootCause = rootCause(e);
        return formatException(rootCause);
    }

    /**
     * Formatea el mensaje de la excepci&oacute;n de acuerdo al tama&ntilde;o total del texto final generado definido
     * por {@link ExceptionUtil#THRESHOLD_COMPLETE_MESSAGE}. Si el largo del texto supera el umbral, intentar&aacute;
     * devolver una cadena m&aacute;s reducida del error sacrificando detalles como por ejemplo el nombre de la
     * funci&oacute;n donde se ubic&oacute; el error o bien el paquete de procedencia de la clase. El formato es el
     * siguiente:<br><br>
     * <b>Versi&oacute;n normal</b>:<br>
     * <code>
     * {ExceptionName} - {CompleteClassName}:{LineException} [M: {MethodName}]
     * </code>
     * <br><br>
     * <b>Versi&oacute;n simple</b>:<br>
     * <code>
     * {ExceptionName} - {SimpleClassName}:{LineException} [M: {MethodName}]
     * </code>
     * <br><br>
     * <b>Versi&oacute;n m&iacute;nima</b>:<br>
     * <code>
     * {ExceptionName} - {SimpleClassName}:{LineException}
     * </code>
     *
     * @param e Excepci&oacute;n a formatear.
     * @param <X> Tipo de excepci&oacute;n.
     * @return El formato del mensaje a mostrar.
     */
    public static <X extends Throwable> String formatException(X e) {
        final StackTraceElement stackTraceElement = obtainFirstStacktraceElementOf(e);
        final String exceptionName = obtainExceptionName(e);
        final String methodName = obtainMethodName(stackTraceElement);
        final String lineException = obtainLineException(stackTraceElement);
        final String normalFormat = String.format("%s - %s:%s [M: %s]",
            exceptionName,
            obtainClassName(stackTraceElement),
            lineException,
            methodName);
        final String reduceFormat = String.format("%s - %s:%s",
            exceptionName,
            formatSimpleClassName(stackTraceElement),
            lineException);
        return (normalFormat.length() < THRESHOLD_COMPLETE_MESSAGE)
            ? normalFormat
            : reduceFormat;
    }

    /**
     * Causa ra&iacute;z del porqu&eacute; ocurri&oacute; una excepci&oacute;n. En caso que la excepci&oacute;n no tenga
     * causas, entonces la excepci&oacute;n por si misma es la causa del problema.
     *
     * @param e Excepci&oacute;n a evaluar
     * @param <X> Tipo de la excepci&oacute;n, se excluyen los {@link Error}
     * @return La causa de la excepci&oacute;n.
     */
    public static <X extends Exception> Throwable rootCause(X e) {
        return Optional.ofNullable(e)
            .map(Throwable::getCause)
            .map(ExceptionUtil::findRootCause0)
            .orElse(e);
    }

    /**
     * Busca la causa ra&iacute;z del problema.
     *
     * @param c Causas de la excepci&oacute;n.
     * @return La causa ra&iacute;z.
     */
    private static Throwable findRootCause0(Throwable c) {
        while (c.getCause() != null) {
            c = c.getCause();
        }
        return c;
    }

    /**
     * Obtiene el nombre simple de la excepci&oacute;n.
     *
     * @param e Excepci&oacute;n a extraer el nombre.
     * @param <X> Tipo de la excepci&oacute;n.
     * @return El nombre simple de la excepci&oacute;n.
     */
    private static <X extends Throwable> String obtainExceptionName(X e) {
        return Optional.ofNullable(e)
            .map(x -> x.getClass().getSimpleName())
            .orElse("?");
    }

    /**
     * Obtiene el n&uacute;mero de l&iacute;nea donde se ubica la excepci&oacute;n.
     *
     * @param stackTraceElement Elemento de la traza de la excepci&oacute;n.
     * @return La l&iacute;nea espec&iacute;fica donde se gatill&oacute; la excepci&oacute;n.
     */
    private static String obtainLineException(final StackTraceElement stackTraceElement) {
        return Optional.ofNullable(stackTraceElement)
            .map(t -> String.valueOf(t.getLineNumber()))
            .orElse("--");
    }

    /**
     * Obtiene el nombre de la funci&oacute;n donde se ubica la excepci&oacute;n.
     *
     * @param stackTraceElement Elemento de la traza de la excepci&oacute;n.
     * @return El nombre de la funci&oacute;n donde se gatill&oacute; la excepci&oacute;n.
     */
    private static String obtainMethodName(final StackTraceElement stackTraceElement) {
        return Optional.ofNullable(stackTraceElement)
            .map(StackTraceElement::getMethodName)
            .orElse("UnknownMethod");
    }

    /**
     * Obtiene el nombre de la clase donde se origin&oacute; la excepci&oacute;n.
     *
     * @param stackTraceElement Elemento de la traza de la excepci&oacute;n.
     * @return El nombre de la clase (completo) donde se gatill&oacute; la excepci&oacute;n.
     */
    private static String obtainClassName(final StackTraceElement stackTraceElement) {
        return Optional.ofNullable(stackTraceElement)
            .map(ExceptionUtil::formatClassName)
            .orElse("UnknownClass");
    }

    /**
     * Entrega el nombre calificado completo de la clase quien gatill&oacute; la excepci&oacute;n, en caso que su nombre
     * sea demasiado largo con un umbral detel.-rminado en {@link ExceptionUtil#THRESHOLD_FULLY_QUALIFIED_CLASS_NAME}
     * entonces mostrar&aacute; el nombre del archivo qui&eacute;n caus&oacute; el error omitiendo el paquete al cual
     * pertenece.
     *
     * @param stackTraceElement Elemento de la traza de la excepci&oacute;n.
     * @return El nombre de la clase o del c&oacute;digo fuente qui&eacute;n lanz&oacute; la excepci&oacute;n.
     */
    private static String formatClassName(StackTraceElement stackTraceElement) {
        return (stackTraceElement.getClassName().length() > THRESHOLD_FULLY_QUALIFIED_CLASS_NAME)
            ? formatSimpleClassName(stackTraceElement)
            : stackTraceElement.getClassName();
    }

    /**
     * Entrega el nombre simple de la clase quien produce la excepci&oacute;n. Internamente entrega el nombre del
     * fichero .java si es que se ubica dentro o fuera del sistema por lo cual pierde informaci&oacute;n del paquete
     * donde se encuentra y en ciertas ocaciones producir confusi&oacute;n si en un sistema se manejan dos o m&aacute;s
     * clases con el mismo nombre.
     *
     * @param stackTraceElement Elemento de la traza de la excepci&oacute;n.
     * @return El nombre simple de la clase o fichero .java quien gatilla la excepci&oacute;n.
     */
    private static String formatSimpleClassName(StackTraceElement stackTraceElement) {
        return Optional.ofNullable(stackTraceElement)
            .map(StackTraceElement::getFileName)
            .orElse("UnknownClass");
    }

    /**
     * Obtiene el primer elemento de la traza de error de una excepci&oacute;n (posici&oacute;n 0). En caso que no venga
     * una excepci&oacute;n retornar&aacute; {@code null}.
     *
     * @param e Excepci&oacute;n a obtener.
     * @param <X> Tipo de excepci&oacute;n, en esta porci&oacute;n de c&oacute;digo no se discrimina si es {@link
     * Exception} o {@link Error}.
     * @return El primer elemento de la traza de error.
     */
    private static <X extends Throwable> StackTraceElement obtainFirstStacktraceElementOf(X e) {
        return existStackTrace(e)
            ? e.getStackTrace()[0]
            : null;
    }

    /**
     * Simplifica la pregunta si existe una traza de pila ({@link StackTraceElement}) relacionada con la
     * excepci&oacute;n.
     *
     * @param e Excepci&oacute;n a tratar.
     * @param <X> Tipo de la excepci&oacute;n.
     * @return TRUE: Si existe una StackTrace disponible; FALSE: En caso contrario.
     */
    private static <X extends Throwable> boolean existStackTrace(X e) {
        return Optional.ofNullable(e)
            .flatMap(st -> Optional.ofNullable(st.getStackTrace()))
            .map(st -> st.length > 0)
            .orElse(false);
    }

    /**
     * Construye el mensaje de la excepci&oacute;n, si &eacute;sta no posee uno entonces intentar&aacute; construir el
     * mensaje a partir de la excepci&oacute;n obtenida y el detalle donde se encuentra el problema.
     *
     * @param e Excepci&oacute;n a tratar.
     * @param <X> Tipo de excepci&oacute;n.
     * @return El mensaje de la excepci&oacute;n formateado.
     */
    public static <X extends Exception> String buildMessage(X e) {
        return buildMessage(e, ErrorType.UNDEFINED);
    }

    /**
     * Construye el mensaje de la excepci&oacute;n, si &eacute;sta no posee uno entonces intentar&aacute; construir el
     * mensaje a partir de la excepci&oacute;n obtenida y el detalle donde se encuentra el problema. Adicionalmente se
     * le puede indicar que tipo de excepci&oacute;n es; que puede ser t&eacute;cnico o funcional, si no se especifica
     * ninguno ({@code null} o {@code ""}) entonces lo omitir&aacute;.
     *
     * @param e Excepci&oacute;n a tratar.
     * @param errorType Tipo de error {@link ErrorType}.
     * @param <X> Tipo de excepci&oacute;n.
     * @return El mensaje de la excepci&oacute;n formateado.
     */
    public static <X extends Exception> String buildMessage(X e, ErrorType errorType) {
        final String message = Optional.ofNullable(e)
            .map(Throwable::getMessage)
            .filter(m -> !m.isEmpty())
            .orElse(formattedRootCause(e));
        return Optional.ofNullable(errorType)
            .map(e0 -> "(" + e0.getDesc().toUpperCase() + ")")
            .map(e0 -> e0 + " " + message)
            .map(String::trim)
            .orElse(message);
    }
}
