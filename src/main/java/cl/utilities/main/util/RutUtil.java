package cl.utilities.main.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.Objects;
import java.util.Optional;

/**
 * Clase utilitaria que realiza diferentes operaciones sobre un RUT.
 *
 * @author Sebastián Gómez
 */
public final class RutUtil {

    private RutUtil() {
        throw new AssertionError(
            "This class is a utilitary, you can't get a instance for this class: " + this.getClass().getName());
    }

    /**
     * Valida que un RUT posea una estructura y valores v&aacute;lidos.
     *
     * @param rut El RUT a evaluar, la estructura de entrada es con d&iacute;gito verificador <b><i>12345678-9</i></b> o
     * bien <b><i>12.345.678-9</i></b>
     * @return <b>true</b>: Si el RUT es v&aacute;lido.<br><b>false</b>: En caso contrario.
     */
    public static boolean isValid(final String rut) {
        return Optional.ofNullable(rut)
            .filter(RutUtil::isValidStructural)
            .map(r -> r.split("-"))
            .map(r -> RutUtil.buildCheckDigit(r[0].trim()).equalsIgnoreCase(r[1].trim()))
            .orElse(false);
    }

    /**
     * Valida que un RUT posea una estructura y valores v&aacute;lidos.
     *
     * @param rut El RUT a evaluar, sin d&iacute;gito verificador.
     * @param checkDigit El d&iacute;gito verificador.
     * @return <b>true</b>: Si el RUT es v&aacute;lido.<br><b>false</b>: En caso contrario.
     */
    public static boolean isValid(final String rut, final String checkDigit) {
        return Optional.of(isValidStructural(rut, checkDigit))
            .filter(i -> i)
            .map(i -> RutUtil.buildCheckDigit(rut.trim()).equalsIgnoreCase(checkDigit.trim()))
            .orElse(false);
    }

    /**
     * Construye el d&iacute;gito verificador de un RUT determinado, si el RUT enviado posee un d&iacute;gito
     * verificador o es nulo entonces lanzar&aacute; la excepci&oacute;n {@code IllegalArgumentException}.
     *
     * @param rut La primera parte del RUT.
     * @return Su d&iacute;gito verificador.
     */
    public static String buildCheckDigit(final String rut) {
        final String rutValidate = Optional.ofNullable(rut)
            .filter(r -> Objects.equals(r.split("-").length, 1))
            .map(r -> r.replace(".", ""))
            .filter(NumberUtils::isDigits)
            .orElseThrow(() -> new IllegalArgumentException("The RUT is not valid or has a check digit: " + rut));
        int validationSeries = 2;
        int sumDigits = 0;
        int moduleRut;
        int numericRut = Integer.parseInt(rutValidate);
        String checkDigit;
        while (numericRut > 0) {
            sumDigits += (numericRut % 10) * validationSeries;
            numericRut /= 10;
            validationSeries++;
            if (validationSeries == 8) {
                validationSeries = 2;
            }
        }
        moduleRut = sumDigits % 11;
        checkDigit = String.valueOf(11 - moduleRut);
        switch (checkDigit) {
            case "10":
                checkDigit = "K";
                break;
            case "11":
                checkDigit = "0";
                break;
            default:
                break;
        }
        return checkDigit;
    }

    /**
     * Valida que el RUT tenga la siguiente estructura: <b><i>numero</i></b>-<b><i>digitoVerificador</i></b>.
     * &Eacute;ste s&oacute;lo se limita a realizar una validaci&oacute;n estructural, mas no si su valor y
     * sud&iacute;gito verificador son v&aacute;lidos, para ello usar {@link RutUtil#isValid(String)}.
     *
     * @param rut RUT a validar.
     * @param checkDigit Dígito verificador.
     * @return <b>true</b>: Si la estructura del RUT es v&aacute;lida.<br><b>false</b>: En caso contrario.
     */
    public static boolean isValidStructural(final String rut, final String checkDigit) {
        return Optional.of(Optional.ofNullable(rut).isPresent() && Optional.ofNullable(checkDigit).isPresent())
            .filter(i -> i)
            .map(i -> "".concat(rut).concat("-").concat(checkDigit))
            .map(RutUtil::isValidStructural)
            .orElse(false);
    }

    /**
     * Valida que el RUT tenga la siguiente estructura: <b><i>numero</i></b>-<b><i>digitoVerificador</i></b>.
     * &Eacute;ste s&oacute;lo se limita a realizar una validaci&oacute;n estructural, mas no si su valor y su
     * d&iacute;gito verificador son v&aacute;lidos, para ello usar {@link RutUtil#isValid(String)}.
     *
     * @param rut RUT a validar.
     * @return <b>true</b>: Si la estructura del RUT es v&aacute;lida.<br><b>false</b>: En caso contrario.
     */
    public static boolean isValidStructural(final String rut) {
        return Optional.ofNullable(rut)
            .filter(StringUtils::isNotBlank)
            .filter(r -> r.split("-").length == 2)
            .filter(r -> Objects.equals(r.split("-")[1].length(), 1))
            .map(r -> r.replace(".", ""))
            .filter(r -> NumberUtils.isDigits(r.split("-")[0]))
            .isPresent();
    }

    /**
     * Obtiene el RUT sin el d&iacute;gito verificador para un RUT v&aacute;lido. Esta funci&oacute;n previamente pasa a
     * validarlo, para comprobar si aplica o no. En caso de no aplicar, retorna un {@code null}.
     *
     * @param rut RUT con d&iacute;gito verificado.
     * @return RUT sin d&iacute;gito verificador.
     */
    public static String obtainWithoutCheckDigit(final String rut) {
        return Optional.ofNullable(rut)
            .map(RutUtil::formatWithHyphenCheckDigit)
            .map(r -> r.split("-")[0])
            .orElse(null);
    }

    /**
     * Obtiene el d&iacute;gito verificador de un RUT v&aacute;lido. Esta funci&oacute;n previamente pasa a validarlo,
     * para comprobar si aplica o no. En caso de no aplicar, retorna un {@code null}.
     *
     * @param rut RUT con d&iacute;gito verificado.
     * @return D&iacute;gito verificador del RUT.
     */
    public static String obtainCheckDigit(final String rut) {
        return Optional.ofNullable(rut)
            .filter(RutUtil::isValid)
            .map(r -> r.split("-")[1])
            .orElse(null);
    }

    /**
     * Formatea el RUT con el gui&oacute;n y su d&iacute;gito verificador. Ejemplo: {@code 12.345.678-9 -> 12345678-9}
     * en caso que el RUT sea inv&aacute;lido, &eacute;ste retornar&aacute; un {@code null}.
     *
     * @param rut RUT v&aacute;lido.
     * @return El RUT formateado con el gui&oacute;n y su d&iacute;gito verificador. {@code null} en caso que sea
     * inv&aacute;lido.
     */
    public static String formatWithHyphenCheckDigit(final String rut) {
        return Optional.ofNullable(rut)
            .filter(RutUtil::isValid)
            .map(r -> r.replace(".", ""))
            .orElse(null);
    }

    /**
     * Formatea el RUT sin el gui&oacute;n y su d&iacute;gito verificador. Ejemplo: {@code 12.345.678-9 -> 123456789} en
     * caso que el RUT sea inv&aacute;lido, &eacute;ste retornar&aacute; un {@code null}.
     *
     * @param rut RUT v&aacute;lido.
     * @return El RUT formateado con el gui&oacute;n sin su d&iacute;gito verificador. {@code null} en caso que sea
     * inv&aacute;lido.
     */
    public static String formatWithoutHyphenCheckDigit(final String rut) {
        return Optional.ofNullable(rut)
            .filter(RutUtil::isValid)
            .map(r -> r.replace(".", ""))
            .map(r -> r.replace("-", ""))
            .orElse(null);
    }
}
