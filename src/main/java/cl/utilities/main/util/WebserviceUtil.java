package cl.utilities.main.util;

import cl.utilities.main.exception.WebserviceUtilException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.HttpHeaders;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Utilitario para los servicios web (<i>Web services</i>) que involucran comunicaci&oacute;n y tratamiento de datos.
 *
 * @author Sebastián Gómez
 */
public final class WebserviceUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebserviceUtil.class);

    /**
     * Agente de usuario para las peticiones GET.
     */
    private static final String USER_AGENT = "Mozilla/5.0";

    /**
     * Constructor privado y que impide la creaci&oacute;n de instancias para este utilitario.
     */
    private WebserviceUtil() {
        throw new AssertionError(
            "You can't get an instance for \"" + this.getClass().getName() + "\", because is a utilitary");
    }

    /**
     * Genera una petici&oacute;n POST a un endpoint especificado usando par&aacute;metros en la URL.
     *
     * @param serviceURL Endpoint al cual se quiere apuntar.
     * @param headers Cabeceras adicionales. No considerar Content-Type.
     * @param mediaType Tipo de petici&oacute;n, éstas pueden ser {@code APPLICATION_JSON, APPLICATION_XML}, etc.
     * @param readTimeout Tiempo de espera antes de terminar de leer y hacer conexi&oacute;n al destino (en
     * <b>milisegundos</b>).
     * @param connectTimeout Tiempo de espera antes de cerrar la conexi&oacute;n (en <b>milisegundos</b>).
     * @param params Lista de par&aacute;metros a la url. En caso de no llevar, usar {@link
     * WebserviceUtil#doPost(String, Map, String, Integer, Integer, Object, Class)}
     * @param objInput Objeto a enviar.
     * @param typeOutput Clase a la que pertenece la respuesta o el response que va a recibir.
     * @param <T> Tipo al que pertenece el objeto de salida (response).
     * @return Una instancia del objeto response o de salida.
     */
    public static <T> T doPost(String serviceURL,
                               Map<String, String> headers,
                               String mediaType,
                               Integer readTimeout,
                               Integer connectTimeout,
                               Map<String, String> params,
                               Object objInput,
                               Class<T> typeOutput) throws WebserviceUtilException {
        serviceURL = Optional.ofNullable(serviceURL)
            .filter(s -> !s.isEmpty())
            .orElseThrow(() -> new IllegalArgumentException("The Service URL cannot be null or empty"));
        final String serviceURLWithParameters = Optional.ofNullable(params)
            .filter(p -> !p.isEmpty())
            .map(p -> p.entrySet().stream()
                .filter(Objects::nonNull)
                .map(e -> e.getKey().concat("=").concat(e.getValue()))
                .map(s -> s.replace(" ", "%20"))
                .collect(Collectors.joining("&")))
            .map("?"::concat)
            .map(serviceURL::concat)
            .orElse(serviceURL);
        return doPost(serviceURLWithParameters, headers, mediaType, readTimeout, connectTimeout, objInput, typeOutput);
    }

    /**
     * Genera una petici&oacute;n POST a un endpoint especificado.
     *
     * @param serviceURL Endpoint al cual se quiere apuntar.
     * @param headers Cabeceras adicionales. No considerar Content-Type.
     * @param mediaType Tipo de petici&oacute;n, éstas pueden ser {@code APPLICATION_JSON, APPLICATION_XML}, etc.
     * @param readTimeout Tiempo de espera antes de terminar de leer y hacer conexi&oacute;n al destino (en
     * <b>milisegundos</b>).
     * @param connectTimeout Tiempo de espera antes de cerrar la conexi&oacute;n (en <b>milisegundos</b>).
     * @param objInput Objeto a enviar.
     * @param typeOutput Clase a la que pertenece la respuesta o el response que va a recibir.
     * @param <T> Tipo al que pertenece el objeto de salida (response).
     * @return Una instancia del objeto response o de salida.
     */
    public static <T> T doPost(String serviceURL,
                               Map<String, String> headers,
                               String mediaType,
                               Integer readTimeout,
                               Integer connectTimeout,
                               Object objInput,
                               Class<T> typeOutput) throws WebserviceUtilException {
        if (StringUtils.isEmpty(serviceURL)) {
            return null;
        }
        WebserviceUtil.evaluateConnectionParams(mediaType,
            readTimeout,
            connectTimeout,
            typeOutput);
        try {
            String jsonOutput = WebserviceUtil.obtainJsonOutput(serviceURL,
                headers,
                HttpPost.METHOD_NAME,
                mediaType,
                readTimeout,
                connectTimeout,
                objInput);
            return JsonUtil.jsonToObject(jsonOutput,
                typeOutput);
        } catch (IOException e) {
            WebserviceUtil.doPostException(serviceURL, objInput, e);
        }
        return null;
    }

    /**
     * Eval&uacute;a los par&aacute;metros de conexi&oacute;n para que ninguno de ellos sea nulo y as&iacute; garantizar
     * la funcionalidad y su conectividad.
     *
     * @param params Par&aacute;metros a evaluar que ninguno de ellos sea null.
     */
    private static void evaluateConnectionParams(Object... params) {
        Objects.requireNonNull(params, "The list of params cannot be null");
        Arrays.stream(params)
            .forEach(p -> Objects.requireNonNull(p,
                "The connection could not be performed because one or some of the parameters are null."));
    }

    /**
     * Obtiene el JSON de salida que env&iacute;a el servicio.
     *
     * @param serviceURL Endpoint al cual se quiere apuntar.
     * @param headers Cabeceras adicionales. No considerar Content-Type.
     * @param httpMethodName Indica si es una petici&oacute;n HTTP <i>GET</i>, <i>POST</i>, <i>PUT</i>, <i>DELETE</i>,
     * etc.
     * @param mediaType Tipo de petici&oacute;n, éstas pueden ser {@code APPLICATION_JSON, APPLICATION_XML}, etc.
     * @param readTimeout Tiempo de espera antes de terminar de leer y hacer conexi&oacute;n al destino (en
     * <b>milisegundos</b>).
     * @param connectTimeout Tiempo de espera antes de cerrar la conexi&oacute;n (en <b>milisegundos</b>).
     * @param objInput Objeto a enviar.
     * @return Un JSON de salida que corresponde a la respuesta del servicio.
     * @throws IOException En caso que haya un problema de escritura con los datos de <i>salida</i>.
     */
    private static String obtainJsonOutput(String serviceURL,
                                           Map<String, String> headers,
                                           String httpMethodName,
                                           String mediaType,
                                           Integer readTimeout,
                                           Integer connectTimeout,
                                           Object objInput) throws IOException {
        URL url = new URL(serviceURL);
        HttpURLConnection connection = WebserviceUtil.getHttpURLConnection(url,
            headers,
            httpMethodName,
            mediaType,
            readTimeout,
            connectTimeout,
            objInput);
        InputStreamReader inputStreamReader = new InputStreamReader(connection.getInputStream());
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        return bufferedReader.lines().collect(Collectors.joining());
    }

    /**
     * Prepara una conexi&oacute;n y ejecuta el servicio para obtener una respuesta por parte de &eacute;ste.
     *
     * @param url Objeto URL donde contiene el endpoint del servicio.
     * @param headers Cabeceras adicionales. No considerar Content-Type.
     * @param httpMethodName Indica si es una petici&oacute;n HTTP <i>GET</i>, <i>POST</i>, <i>PUT</i>, <i>DELETE</i>,
     * etc.
     * @param mediaType Tipo de petici&oacute;n, éstas pueden ser {@code APPLICATION_JSON, APPLICATION_XML}, etc.
     * @param readTimeout Tiempo de espera antes de terminar de leer y hacer conexi&oacute;n al destino (en
     * <b>milisegundos</b>).
     * @param connectTimeout Tiempo de espera antes de cerrar la conexi&oacute;n (en <b>milisegundos</b>).
     * @param objInput Objeto a enviar, no aplica para conexiones de tipo <i>GET</i>, para ello se env&iacute;a un
     * <code>null</code>.
     * @return La conexi&oacute;n con los datos del servicio y el objeto incluido de llegada.
     * @throws IOException En caso que haya un problema de escritura con los datos de <i>entrada</i>.
     */
    private static HttpURLConnection getHttpURLConnection(URL url,
                                                          Map<String, String> headers,
                                                          String httpMethodName,
                                                          String mediaType,
                                                          Integer readTimeout,
                                                          Integer connectTimeout,
                                                          Object objInput) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setReadTimeout(readTimeout);
        connection.setConnectTimeout(connectTimeout);
        connection.setRequestMethod(httpMethodName);
        connection.setRequestProperty(HttpHeaders.CONTENT_TYPE, mediaType);
        Optional.ofNullable(headers).ifPresent(h -> h.forEach(connection::setRequestProperty));
        connection.setDoOutput(true);
        connection.setDoInput(true);
        switch (httpMethodName.toUpperCase()) {
            case "GET":
                connection.setRequestProperty("User-Agent", WebserviceUtil.USER_AGENT);
                break;
            case "POST":
                buildPostSettingsConnection(objInput, connection);
                break;
            case "PUT":
            case "DELETE":
                break;
            default:
                throw new IOException("The HTTP method cannot be readed or recognized.");
        }
        return connection;
    }

    /**
     * Construye las propiedades de la conexi&oacute;n para hacer un POST.
     *
     * @param objInput Objeto de entrada a convertir en JSON.
     * @param connection Objeto de conexi&oacute;n para setear las propiedades en particular para una petici&oacute;n
     * POST.
     * @throws IOException En caso que el OutputStream falle.
     */
    private static void buildPostSettingsConnection(Object objInput, HttpURLConnection connection) throws IOException {
        Objects.requireNonNull(objInput, "The input object cannot be null");
        OutputStream outputStream = connection.getOutputStream();
        outputStream.write(JsonUtil.objectToJson(objInput).getBytes());
        outputStream.flush();
        outputStream.close();
    }

    /**
     * @param serviceURL Endpoint al cual se quiere apuntar.
     * @param objInput Objeto a enviar.
     * @param e Excepci&oacute;n.
     * @throws WebserviceUtilException Excepci&oacute;n encapsulada a lanzar.
     */
    private static void doPostException(String serviceURL, Object objInput, IOException e) throws WebserviceUtilException {
        final String errorMessage = "Error while trying do a POST in "
            .concat(serviceURL)
            .concat(", with object: ")
            .concat(objInput.toString())
            .concat(", I/O cause: ")
            .concat(e.getMessage());
        LOGGER.error(errorMessage);
        throw new WebserviceUtilException(errorMessage);
    }

    /**
     * Genera una petici&oacute;n GET a un endpoint especificado. Para un servicio que retone una lista de elementos
     * usar la funci&oacute;n {@link WebserviceUtil#doGetList(String, Map, String, Integer, Integer, List, Class)}
     *
     * @param serviceURL Endpoint al cual se quiere apuntar.
     * @param headers Cabeceras adicionales. No considerar Content-Type.
     * @param mediaType Tipo de petici&oacute;n, éstas pueden ser {@code APPLICATION_JSON, APPLICATION_XML}, etc.
     * @param readTimeout Tiempo de espera antes de terminar de leer y hacer conexi&oacute;n al destino (en
     * <b>milisegundos</b>).
     * @param connectTimeout Tiempo de espera antes de cerrar la conexi&oacute;n (en <b>milisegundos</b>).
     * @param params Lista de par&aacute;metros a la url. Esta puede ser <b>null</b>, en caso de no llevar ninguno.
     * @param typeOutput Clase a la que pertenece la respuesta o el response que va a generar el microservicio.
     * @param <T> Tipo al que pertenece el objeto de salida (response).
     * @return Una instancia del objeto response o de salida.
     */
    public static <T> T doGet(String serviceURL,
                              Map<String, String> headers,
                              String mediaType,
                              Integer readTimeout,
                              Integer connectTimeout,
                              List<String> params,
                              Class<T> typeOutput) throws WebserviceUtilException {
        if (StringUtils.isEmpty(serviceURL)) {
            return null;
        }
        try {
            String jsonOutput = WebserviceUtil.obtainGetJsonOutput(serviceURL,
                headers,
                mediaType,
                readTimeout,
                connectTimeout,
                params,
                typeOutput);
            return JsonUtil.jsonToObject(jsonOutput, typeOutput);
        } catch (IOException e) {
            WebserviceUtil.doGetException(serviceURL, e);
        }
        return null;
    }

    /**
     * Obtiene el JSON de salida generado por el servicio.
     *
     * @param serviceURL Endpoint al cual se quiere apuntar.
     * @param headers Cabeceras adicionales. No considerar Content-Type.
     * @param mediaType Tipo de petici&oacute;n, éstas pueden ser {@code APPLICATION_JSON, APPLICATION_XML}, etc.
     * @param readTimeout Tiempo de espera antes de terminar de leer y hacer conexi&oacute;n al destino (en
     * <b>milisegundos</b>).
     * @param connectTimeout Tiempo de espera antes de cerrar la conexi&oacute;n (en <b>milisegundos</b>).
     * @param params Lista de par&aacute;metros a la url. Esta puede ser <b>null</b>, en caso de no llevar ninguno.
     * @param typeElementsInList Tipo de objetos que est&aacute;n contenidos en las listas.
     * @param <T> Tipo al que pertenece el objeto de salida (response).
     * @return JSON de salida mediante la petici&oacute;n GET.
     * @throws IOException En caso que el JSON sea inv&aacute;lido.
     */
    private static <T> String obtainGetJsonOutput(String serviceURL,
                                                  Map<String, String> headers,
                                                  String mediaType,
                                                  Integer readTimeout,
                                                  Integer connectTimeout,
                                                  List<String> params,
                                                  Class<T> typeElementsInList) throws IOException {
        WebserviceUtil.evaluateConnectionParams(mediaType,
            readTimeout,
            connectTimeout,
            typeElementsInList);
        serviceURL = CollectionUtils.isEmpty(params)
            ? serviceURL
            : serviceURL.concat("/").concat(String.join("/",
                params));
        return WebserviceUtil.obtainJsonOutput(serviceURL,
            headers,
            HttpGet.METHOD_NAME,
            mediaType,
            readTimeout,
            connectTimeout,
            null);
    }

    /**
     * Muestra la excepci&oacute;n generada mientras se realiza una llamada GET.
     *
     * @param serviceURL Endpoint al cual se quiere apuntar.
     * @param e Excepci&oacute;n.
     * @throws WebserviceUtilException Excepci&oacute;n encapsulada a lanzar.
     */
    private static void doGetException(String serviceURL, IOException e) throws WebserviceUtilException {
        final String errorMessage = "Error while trying do a GET in "
            .concat(serviceURL)
            .concat(". ")
            .concat("Cause: ")
            .concat(e.getMessage());
        LOGGER.error(errorMessage);
        throw new WebserviceUtilException(errorMessage);
    }

    /**
     * Genera una petici&oacute;n POST a un endpoint especificado cuando es un objeto de tipo {@link List}.
     *
     * @param serviceURL Endpoint al cual se quiere apuntar.
     * @param headers Cabeceras adicionales. No considerar Content-Type.
     * @param mediaType Tipo de petici&oacute;n, éstas pueden ser {@code APPLICATION_JSON, APPLICATION_XML}, etc.
     * @param readTimeout Tiempo de espera antes de terminar de leer y hacer conexi&oacute;n al destino (en
     * <b>milisegundos</b>).
     * @param connectTimeout Tiempo de espera antes de cerrar la conexi&oacute;n (en <b>milisegundos</b>).
     * @param objInput Objeto a enviar.
     * @param typeElementsInList Tipo de objetos que est&aacute;n contenidos en las listas.
     * @param <T> Tipo al que pertenece el objeto de salida (response).
     * @return Una lista del objeto response o de salida.
     */
    public static <T> List<T> doPostList(String serviceURL,
                                         Map<String, String> headers,
                                         String mediaType,
                                         Integer readTimeout,
                                         Integer connectTimeout,
                                         Object objInput,
                                         Class<T> typeElementsInList) throws WebserviceUtilException {
        if (StringUtils.isEmpty(serviceURL)) {
            return Collections.emptyList();
        }
        WebserviceUtil.evaluateConnectionParams(mediaType,
            readTimeout,
            connectTimeout,
            typeElementsInList);
        try {
            String jsonOutput = WebserviceUtil.obtainJsonOutput(serviceURL,
                headers,
                HttpPost.METHOD_NAME,
                mediaType,
                readTimeout,
                connectTimeout,
                objInput);
            return JsonUtil.jsonToObjectList(jsonOutput, typeElementsInList);
        } catch (IOException e) {
            WebserviceUtil.doPostException(serviceURL, objInput, e);
        }
        return Collections.emptyList();
    }

    /**
     * Genera una petici&oacute;n GET a un endpoint especificado que retorna una lista o colecci&oacute;n de elementos.
     *
     * @param serviceURL Endpoint al cual se quiere apuntar.
     * @param headers Cabeceras adicionales. No considerar Content-Type.
     * @param mediaType Tipo de petici&oacute;n, éstas pueden ser {@code APPLICATION_JSON, APPLICATION_XML}, etc.
     * @param readTimeout Tiempo de espera antes de terminar de leer y hacer conexi&oacute;n al destino (en
     * <b>milisegundos</b>).
     * @param connectTimeout Tiempo de espera antes de cerrar la conexi&oacute;n (en <b>milisegundos</b>).
     * @param params Lista de par&aacute;metros a la url. Esta puede ser <b>null</b>, en caso de no llevar ninguno.
     * @param typeElementsInList Tipo de objetos que est&aacute;n contenidos en las listas.
     * @param <T> Tipo al que pertenece el objeto de salida (response).
     * @return Una lista del objeto response o de salida.
     */
    public static <T> List<T> doGetList(String serviceURL,
                                        Map<String, String> headers,
                                        String mediaType,
                                        Integer readTimeout,
                                        Integer connectTimeout,
                                        List<String> params,
                                        Class<T> typeElementsInList) throws WebserviceUtilException {
        if (StringUtils.isEmpty(serviceURL)) {
            return Collections.emptyList();
        }
        try {
            String jsonOutput = obtainGetJsonOutput(serviceURL,
                headers,
                mediaType,
                readTimeout,
                connectTimeout,
                params,
                typeElementsInList);
            return JsonUtil.jsonToObjectList(jsonOutput, typeElementsInList);
        } catch (IOException e) {
            WebserviceUtil.doGetException(serviceURL, e);
        }
        return Collections.emptyList();
    }
}
