package cl.utilities.main.util;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Clase utilitaria para manejar o mejorar los utilitarios de los strings. Especialmente cadenas de entradas variables y
 * de conformaci&oacute;n de cadenas gen&eacute;ricas. No se ha nombrado <code>StringUtil</code> para no confundirlo con
 * {@link org.apache.commons.lang3.StringUtils} que a su vez es amplamente utilizado dentro de este microservicio.
 *
 * @author Sebastián Gómez
 */
public final class StringCommonUtil {

    /**
     * Constructor privado y que impide la creaci&oacute;n de instancias para este utilitario.
     */
    private StringCommonUtil() {
        throw new AssertionError(
            "You can't get an instance for \"" + this.getClass().getName() + "\", because is a utilitary");
    }

    /**
     * Junta una cantidad variable de strings tolerante a valores nulos y vac&iacute;os, cuyos valores no se
     * incluir&aacute;n en la secuencia final, a diferencia de la funci&oacute;n nativa {@link String#join(CharSequence,
     * CharSequence...)} que si los incluye.
     *
     * @param delimiter Separador o delimitador de caracteres que juntar&aacute;n los strings.
     * @param strings Strings a juntar.
     * @return Cadena de caracteres unidos.
     * @see String#join(CharSequence, CharSequence...)
     */
    public static String join(CharSequence delimiter, CharSequence... strings) {
        Objects.requireNonNull(delimiter, "The delimiter cannot be null");
        Objects.requireNonNull(strings, "The strings to concatenate cannot be null");
        return Arrays.stream(strings)
            .filter(Objects::nonNull)
            .filter(s -> !s.toString().trim().isEmpty())
            .collect(Collectors.joining(delimiter));
    }

    /**
     * Convierte un {@link String} en su representaci&oacute;n en may&uacute;sculas, en caso que sea {@code null},
     * retornar&aacute; {@code null}.
     *
     * @param str String a convertir a may&uacute;sculas.
     * @return La representacio&oacute;n del string en may&uacute;sculas.
     */
    public static String toUpperCase(CharSequence str) {
        return Optional.ofNullable(str)
            .map(CharSequence::toString)
            .map(String::toUpperCase)
            .orElse(null);
    }

    /**
     * Convierte una {@link List} de {@link String} en lista de strings en min&uacute;sculas.
     *
     * @param strings Lista de strings.
     * @return Una lista de strings en may&uacute;sculas.
     */
    public static List<String> toUpperCase(List<? extends CharSequence> strings) {
        Objects.requireNonNull(strings, "The string list cannot be null");
        return strings.stream()
            .map(StringCommonUtil::toUpperCase)
            .collect(Collectors.toList());
    }

    /**
     * Convierte varios {@link String} en una {@link List} de {@link String} en min&uacute;sculas.
     *
     * @param strings Varags de Strings.
     * @return Una lista de strings en may&uacute;sculas.
     */
    public static List<String> toUpperCase(CharSequence... strings) {
        Objects.requireNonNull(strings, "The string args cannot be null");
        return Arrays.stream(strings)
            .map(StringCommonUtil::toUpperCase)
            .collect(Collectors.toList());
    }

    /**
     * Convierte un {@link String} en su representaci&oacute;n en min&uacute;sculas, en caso que sea {@code null},
     * retornar&aacute; {@code null}.
     *
     * @param str String a convertir a may&uacute;sculas.
     * @return La representaci&oacute;n del string en may&uacute;sculas.
     */
    public static String toLowerCase(CharSequence str) {
        return Optional.ofNullable(str)
            .map(CharSequence::toString)
            .map(String::toLowerCase)
            .orElse(null);
    }

    /**
     * Convierte una {@link List} de {@link String} en lista de strings en min&uacute;sculas.
     *
     * @param strings Lista de strings.
     * @return Una lista de strings en may&uacute;sculas.
     */
    public static List<String> toLowerCase(List<? extends CharSequence> strings) {
        Objects.requireNonNull(strings, "The string list cannot be null");
        return strings.stream()
            .map(StringCommonUtil::toLowerCase)
            .collect(Collectors.toList());
    }

    /**
     * Convierte varios {@link String} en una {@link List} de {@link String} en min&uacute;sculas.
     *
     * @param strings Varags de Strings.
     * @return Una lista de strings en may&uacute;sculas.
     */
    public static List<String> toLowerCase(CharSequence... strings) {
        Objects.requireNonNull(strings, "The string args cannot be null");
        return Arrays.stream(strings)
            .map(StringCommonUtil::toLowerCase)
            .collect(Collectors.toList());
    }
}
