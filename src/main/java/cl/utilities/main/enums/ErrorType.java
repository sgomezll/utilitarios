package cl.utilities.main.enums;

public enum ErrorType {
    UNDEFINED(""),
    FUNCIONAL("Funcional"),
    TECNICO("Técnico");

    private String desc;

    ErrorType(String desc) {
        this.desc = desc;
    }

    /**
     * Obtiene la descripción del error.
     *
     * @return El valor de la descripción del error.
     */
    public String getDesc() {
        return desc;
    }
}
